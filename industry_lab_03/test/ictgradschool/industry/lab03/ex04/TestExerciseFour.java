package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class TestExerciseFour {

    private ExerciseFour ex;

    @Before
    public void setUp() {
        ex = new ExerciseFour();
    }

    //@Rule
    //public final ExpectedException exception = ExpectedException.none();


    @Test(expected = IllegalArgumentException.class)
    public void testAmountInputException(){
        String ex1 = "3.14.159";
        String ex2 = "3.14.";
        String ex3 = "3.";
        String ex4 = "314";
        System.setIn(new java.io.ByteArrayInputStream(ex1.getBytes()));
    }

    @Test
    public void testDecimalPlaceInput(){

    }

    @Test
    public void testTruncate() {
        assertEquals("3.14", ex.truncateAmount("3.14159", 2));
    }

}
