package ictgradschool.industry.lab03.ex03;

import ictgradschool.Keyboard;
import org.jetbrains.annotations.NotNull;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */
public class ExerciseThree {

    private void start() throws Exception {

        String sentence = getSentenceFromUser();

        int randomPosition = getRandomPosition(sentence);

        printCharacterToBeRemoved(sentence, randomPosition);

        String changedSentence = removeCharacter(sentence, randomPosition);

        printNewSentence(changedSentence);
    }

    /**
     * Gets a sentence from the user.
     * @return user input
     */
    private String getSentenceFromUser() throws Exception {

        // TODO Prompt the user to enter a sentence, then get their input.
        System.out.print("Enter a sentence: ");
        String sentence = Keyboard.readInput();
        if (sentence.length() == 0)
            throw new Exception("You must enter a sentence.");
        return sentence;
    }

    /**
     * Gets an int corresponding to a random position in the sentence.
     */
    private int getRandomPosition(String sentence) {

        // TODO Use a combination of Math.random() and sentence.length() to get the desired result.
        return (int)(Math.random() * (sentence.length()-1));
    }

    /**
     * Prints a message stating the character to be removed, and its position.
     */
    private void printCharacterToBeRemoved(String sentence, int position) {

        // TODO Implement this method
        System.out.println(String.format("Removing \"%s\" from position %d",
                sentence.charAt(position), position));

    }

    /**
     * Removes a character from the given sentence, and returns the new sentence.
     */
    @NotNull
    private String removeCharacter(String sentence, int position) {

        // TODO Implement this method
        return sentence.substring(0, position) + sentence.substring(position+1, sentence.length());

    }

    /**
     * Prints a message which shows the new sentence after the removal has occured.
     */
    private void printNewSentence(String changedSentence) {

        // TODO Implement this method
        System.out.println("New sentence is: " + changedSentence);
    }

    public static void main(String[] args) throws Exception {
        ExerciseThree ex = new ExerciseThree();
        ex.start();
    }
}
