package ictgradschool.industry.lab03.ex06;

public class ExerciseSix {
    // 1
    private char getRandomLetter ( String word ){
        int position = ( int )( Math . random ()* word . length ());
        return word . charAt ( position );
    }

    // 2
    private String getSurname ( String  name ) {
        int positionOfSpace = name . indexOf ( " " );
        return name . substring ( positionOfSpace + 1 );
    }

    // 3
    private double getBMI ( double weight , double height ){
        double bmi = weight / Math . pow ( height , 2 );
        return bmi ;
    }

    // 4
    private void printTemperature ( int degrees ){
        System . out . println ( "The temperature is " + degrees );
    }
}
