package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;


/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        int lowerBound = getUserInput.getInputAsInt("Lower bound?");
        int upperBound = getUserInput.getInputAsInt("Upper bound?");

        int[] threeInts = new int[3];

        for (int i = 0; i < threeInts.length; i++)
            threeInts[i] = getRandom.generateRandomBetween(lowerBound, upperBound);

        System.out.println(String.format("3 randomly generated numbers: %d, %d and %d",
                threeInts[0], threeInts[1], threeInts[2]));

        int smallest = Math.min(Math.min(threeInts[0], threeInts[1]), threeInts[2]);
        System.out.println(String.format("Smallest number is %d", smallest));

    }

    private Func2 getUserInput = (String s) -> {
        System.out.print(s + " ");
        return Integer.parseInt(Keyboard.readInput());
    };

    private Func1 getRandom = (l, u) -> (int)(Math.random()*(u-l+1) + l);

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();
    }
}

@FunctionalInterface
interface Func1 {
    int generateRandomBetween(int a, int b);
}

@FunctionalInterface
interface Func2 {
    int getInputAsInt(String s);
}
