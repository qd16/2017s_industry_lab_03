package ictgradschool.industry.lab03.ex08;

import ictgradschool.Keyboard;

public class ExerciseEight {
    public static void main(String[] args) {
        start();
    }

    public static void start () {
        double radius;
        System . out . println ( "\"Volume of a Sphere\"");
        System.out.print(" Enter the radius : ");
        radius = Double . parseDouble (Keyboard.readInput());
        double volume = 4.0 / 3.0 * Math.PI * Math.pow( radius , 3 );
        System . out . println (String.format("Volume: %f" , volume ));
    }
}
