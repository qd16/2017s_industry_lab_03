package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;
import org.jetbrains.annotations.NotNull;

import java.security.Key;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        // TODO Use other methods you create to implement this program's functionality.
        String amount = readUserInput();
        int decimalPlace = readDecimalPlace(amount);
        String truncated = truncateAmount(amount, decimalPlace);
        printTruncated(truncated);
    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
    public String readUserInput(){
        System.out.print("Please enter an amount: ");
        String amount = Keyboard.readInput();
        if (amount.length() - amount.replace(".", "").length() != 1     // has no or more than 1 dot
                || amount.lastIndexOf('.') == amount.length()-1)                      // dot at the end of string
            throw new IllegalArgumentException("You must enter a valid amount number.");

        return amount;
    }

    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    public int readDecimalPlace(String amount){
        System.out.print("Please enter the number of decimal places: ");
        int dp = Integer.parseInt(Keyboard.readInput());
        if (dp > amount.substring(amount.indexOf('.')).length()-1) {
            throw new IllegalArgumentException("Cannot truncate at this decimal place.");
        }

        return dp;
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's
    @NotNull
    public String truncateAmount(String amount, int dp){
        return dp == 0
                ? amount.substring(0, amount.indexOf('.'))
                : amount.substring(0, amount.indexOf('.')+dp+1);
    }

    // TODO Write a method which prints the truncated amount
    public void printTruncated(String truncated){
        System.out.println("Amount truncated to 1 decimal places is: " + truncated);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
