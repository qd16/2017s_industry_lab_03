package ictgradschool.industry.lab01;

import ictgradschool.industry.lab01.ex03.TestExerciseThree;
import ictgradschool.industry.lab01.ex04.TestExerciseFour;
import ictgradschool.industry.lab01.ex05.TestExerciseFive;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestExerciseThree.class,
        TestExerciseFour.class,
        TestExerciseFive.class
})
public class TestSuite { }
