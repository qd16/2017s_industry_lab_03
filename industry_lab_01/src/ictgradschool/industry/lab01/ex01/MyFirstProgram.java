package ictgradschool.industry.lab01.ex01;

public class MyFirstProgram {
    private void start(String[] names) {
        String words = "Hello ";
        for (String name:names) {
            words += name + " ";
        }
        System.out.println(words.trim() + "!");
    }

    /**
     * All Java programs begin with the method: main().
     * @param args The default arguments.
     */
    public static void main(String[] args) {
        MyFirstProgram p = new MyFirstProgram();
        p.start(args);
    }
}
