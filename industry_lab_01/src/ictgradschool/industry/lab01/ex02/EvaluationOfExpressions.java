package ictgradschool.industry.lab01.ex02;


public class EvaluationOfExpressions {

    public static void main(String[] args){
        System.out.println (( int ) 2.9 * Double . parseDouble ( "4.5" ));
        System.out.println ( "17" + Integer . parseInt ( "2" ) * 3.5 );
        System.out.println ( Math . max ( 5 , 60 ) % Math . min ( 12 , 7 ));
        System.out.println ( 0.2 * 3 / 2 + 3 / 2 * 3.2 );
        System.out.println ( "5 + 3" + 19 % 2 + 19 / 2 );
        System.out.println ( 2 + 5 + "59" + 3 * 2 + ( 3 + 2 ));
    }
}
