package ictgradschool.industry.lab02.ex05;

/**
 * Programming for Industry
 * Lab 02 - Control Flow
 * Exercise Five
 */

public class ExerciseFive {
    public static void main(String[] args) {
        int number = 5 ;
        while ( number < 15 ) {
            System . out . print ( 3 * number + " " );
            number += 4 ;
        }
        System . out . println ();
        System . out . println ( "Number is now: " + number );
    }
}

// 15 27 39
// Number is now 17