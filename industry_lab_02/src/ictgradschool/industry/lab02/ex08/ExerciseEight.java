package ictgradschool.industry.lab02.ex08;

/**
 * Please run TestExerciseNine to check your answers
 */
public class ExerciseEight {

    /**
     * Q1. Compare two names and if they are the same return "Same name",
     * otherwise if they start with exactly the same letter return "Same
     * first letter", otherwise return "No match".
     *
     * @param firstName
     * @param secondName
     *
     * @return one of three Strings: "Same name", "Same first letter",
     * "No match"
     */
    public String areSameName(String firstName, String secondName) {
        String message = "";
        // TODO write answer to Q1
        if (firstName.toLowerCase().equals(secondName.toLowerCase())){
            message = "Same name";
        }
        else if (firstName.toLowerCase().charAt(0) == secondName.toLowerCase().charAt(0)){
            message = "Same first letter";
        }
        else {
            message = "No match";
        }
        return message;
    }
    /** areSameName(String, String) => String **/


    /**
     * Q2. Determine if the given year is a leap year.
     *
     * @param year
     *
     * @return true if the given year is a leap year, false otherwise
     */
    public boolean isALeapYear(int year) {
        boolean leapYear = false;
        // TODO write answer for Q2
        leapYear = year % 100 == 0 ? year % 400 == 0: year % 4 == 0;
        return leapYear;
    }
    /** isALeapYear(int) => boolean **/


    /**
     * Q3. When given an integer, return an integer that is the reverse (its
     * numbers are in reverse to the input).
     * order.
     *
     * @param number
     *
     * @return the integer with digits in reverse order
     */
    public int reverseInt(int number) {
        // TODO write answer for Q3
        boolean positive = number > 0;
        number = Math.abs(number);
        StringBuilder reversed = new StringBuilder();
        while (number > 0){
            reversed.append(String.valueOf(number % 10));
            number /= 10;
        }

        return positive ? Integer.parseInt(reversed.toString()) : -Integer.parseInt(reversed.toString());

    }
    /** reverseInt(int) => void **/


    /**
     * Q4. Return the given string in reverse order.
     *
     * @param str
     *
     * @return the String with characters in reverse order
     */
    public String reverseString(String str) {
        StringBuilder reverseStr = new StringBuilder();
        // TODO write answer for Q4
        for (int i = 1; i <= str.length(); i++){
            reverseStr.append(str.charAt(str.length() - i));
        }

        return reverseStr.toString();
    }
    /** reverseString(String) => void **/


    /**
     * Q5. Generates the simple multiplication table for the given integer.
     * The resulting table should be 'num' columns wide and 'num' rows tall.
     *
     * @param num
     *
     * @return the multiplication table as a newline separated String
     */
    public String simpleMultiplicationTable(int num) {
        StringBuilder multiplicationTable = new StringBuilder();
        // TODO write answer for Q5
        for (int i = 1; i <= num; i++){
            for (int j = 1; j <= num; j++){
                multiplicationTable.append(i * j).append(" ");
            }
            multiplicationTable.append(multiplicationTable.toString().trim()).append('\n');
        }

        return multiplicationTable.toString().trim();
    }
    /** simpleMultiplicationTable(int) => void **/


    /**
     * Q6. Determines the Excel column name of the given column number.
     *
     * @param num
     *
     * @return the column title as a String
     */
    public String convertIntToColTitle(int num) {
        // TODO write answer for Q6
        if (num <= 0)
            return  "Input is invalid";

        StringBuilder sb = new StringBuilder();
        do {
            sb.reverse().append((char)(--num%26 + 65)).reverse();
            num /= 26;
        } while (num > 0);
        return sb.toString();
    }
    /** convertIntToColTitle(int) => void **/


    /**
     * Q7. Determine if the given number is a prime number.
     *
     * @param num
     *
     * @return true is the given number is a prime, false otherwise
     */
    public boolean isPrime(int num) {
        // TODO write answer for Q7
        if (num <= 1)
            return false;

        boolean isPrime = true;
        for (int i = 2; i <= Math.sqrt(num); i++){
            if (num % i == 0){
                isPrime = false;
                break;
            }
        }

        return isPrime;
    }
    /** isPrime(int) => void **/


    /**
     * Q8. Determine if the given integer is a palindrome (ignoring negative
     * sign).
     *
     * @param num
     *
     * @return true is int is palindrome, false otherwise
     */
    public boolean isIntPalindrome(int num) {
        // TODO write answer for Q8

        return num == reverseInt(num);
    }
    /** isIntPalindrom(int) => boolean **/


    /**
     * Q9. Determine if the given string is a palindrome (case folded).
     *
     * @param str
     *
     * @return true if string is palindrome, false otherwise
     */
    public boolean isStringPalindrome(String str) {
        // TODO write answer for Q9
        str = str.replaceAll("\\s", "");
        return str.equals(reverseString(str));
    }
    /** isStringPalindrome(String) => boolean **/


    /**
     * Q10. Generate a space separated list of all the prime numbers from 2
     * to the highest prime less than or equal to the given integer.
     *
     * @param num
     *
     * @return the primes as a space separated list
     */
    public String printPrimeNumbers(int num) {
        if (num <= 1)
            return "No prime number found";

        StringBuilder primesStr = new StringBuilder("2 ");
        // TODO write answer for Q10
        for (int i = 3; i <= num; i+=2){
            if (isPrime(i)){
                primesStr.append(i).append(" ");
            }
        }

        return primesStr.toString().trim();
    }
}
