package ictgradschool.industry.lab02.ex7;
/**
 * Programming for Industry
 * Lab 02 - Control Flow
 * Exercise Seven
 */
public class ExerciseSeven {

    /**
     * Exercise 6: Translate the following while loop into a for loop.
     *
     *
     */
    public static void main(String[] args) {
        for (int i = 0; i < 7; i++){
            System.out.print(2 * i +  3);
        }
    }
}
