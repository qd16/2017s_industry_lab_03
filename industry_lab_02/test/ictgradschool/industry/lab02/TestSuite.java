package ictgradschool.industry.lab02;

import ictgradschool.industry.lab02.ex03.TestExerciseThree;
import ictgradschool.industry.lab02.ex04.TestExerciseFour;
import ictgradschool.industry.lab02.ex06.TestExerciseSix;
import ictgradschool.industry.lab02.ex08.TestExerciseEight;
import org.junit.runners.Suite;
import org.junit.runner.RunWith;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestExerciseThree.class,
        TestExerciseFour.class,
        TestExerciseSix.class,
        TestExerciseEight.class,
        TestExerciseEight.class})
public class TestSuite {}
