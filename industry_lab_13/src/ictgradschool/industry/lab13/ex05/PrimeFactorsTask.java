package ictgradschool.industry.lab13.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable {

    public enum TaskState {
        Initialized, Completed, Aborted
    }

    private long n;
    private List<Long> primeFactors;
    private TaskState taskState;

    public PrimeFactorsTask(long n){
        this.n = n;
        this.primeFactors = new ArrayList<>();
        this.taskState = TaskState.Initialized;
    }

    public List<Long> getPrimeFactors(){
        return primeFactors;
    }

    public long n(){
        return n;
    }

    public TaskState getState(){
        return this.taskState;
    }

    public void Abort(){
        this.taskState = TaskState.Aborted;
    }

    @Override
    public void run() {
        for (long i = 2; i*i < n; i++) {
            while (n % i == 0){
                primeFactors.add(i);
                n /= i;
            }

            // check interrupt status
            if (getState() == TaskState.Aborted){
                System.out.println("User aborted...");
                return;
            }
        }
        if (n > 1){
            primeFactors.add(n);
        }
        taskState = TaskState.Completed;
    }
}
