package ictgradschool.industry.lab13.ex05;


import ictgradschool.Keyboard;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class PrimeFactorApp {

    private Thread calcThread;

    public static void main(String[] args) {
        PrimeFactorApp app = new PrimeFactorApp();
        try {
            app.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private void start() throws InterruptedException {
        System.out.print("Please enter an integer: ");
        long n = Long.parseLong(Keyboard.readInput());

        PrimeFactorsTask pft = new PrimeFactorsTask(n);
        calcThread = new Thread(pft);


        Thread abortThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()){
                    if (Keyboard.readInput() != null) {
                        System.out.println("here");
                        pft.Abort();
                        System.out.println(pft.getState());
                    }
                }
            }
        });


        System.out.println("Calculating prime factors of " + n);
        calcThread.start();
        abortThread.start();

        calcThread.join();
        //abortThread.join();

        System.out.println(pft.getPrimeFactors());
    }

}
