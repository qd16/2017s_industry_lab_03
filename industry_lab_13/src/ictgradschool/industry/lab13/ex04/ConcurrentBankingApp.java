package ictgradschool.industry.lab13.ex04;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {

    private static Thread producer;
    private static Thread[] consumers;

    public static void main(String[] args) throws InterruptedException {

        // Acquire Transactions to process.
        Queue<Transaction> transactions = TransactionGenerator.readDataFile();

        // Create BankAccount object to operate on.
        BankAccount account = new BankAccount();

        /*
        // For each Transaction, apply it to the BankAccount instance.
        Thread[] threads = new Thread[8];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true){
                        Transaction t = transactions.poll();
                        if (t == null)
                            break;
                        System.out.println("amount: " + t._amountInCents);
                        switch (t._type){
                            case Deposit:
                                account.deposit(t._amountInCents);
                                break;
                            case Withdraw:
                                account.withdraw(t._amountInCents);
                                break;
                            default:
                                break;
                        }
                    }
                }
            });
            threads[i].start();
        }

        for (Thread thread : threads) {
            thread.join();
        }
        */

        final BlockingQueue<Transaction> tq = new ArrayBlockingQueue<Transaction>(10);

        producer = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!transactions.isEmpty() && !Thread.currentThread().isInterrupted()){
                    if (tq.remainingCapacity() == 0){
                        System.out.println("Full, interrupt producers");
                        producer.interrupt();

                    } else {
                        tq.add(transactions.poll());
                    }

                    if (!tq.isEmpty()){
                        for (Thread consumer : consumers) {
                            if (!consumer.isAlive()){
                                consumer.start();
                            }
                        }
                    }
                }
            }
        });

        consumers = new Thread[2];

        for (int i = 0; i < consumers.length; i++) {
            consumers[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!Thread.currentThread().isInterrupted()){
                        if (tq.isEmpty()){
                            System.out.println("empty!!!");
                            if (producer.getState() == Thread.State.TERMINATED){
                                producer.start();
                            }
                        }
                        Transaction t = tq.poll();
                        if (t == null)
                            break;

                        System.out.println("Trans amount:\t" + t._amountInCents);
                        switch (t._type){
                            case Deposit:
                                account.deposit(t._amountInCents);
                                break;
                            case Withdraw:
                                account.withdraw(t._amountInCents);
                                break;
                            default:
                                break;
                        }
                    }
                }
            });
        }


        producer.start();
//        for (Thread consumer : consumers) {
//            consumer.start();
//        }

        producer.join();
        for (Thread consumer : consumers) {
            consumer.join();
        }



        // Print the final balance after applying all Transactions.
        System.out.println("Final balance: " + account.getFormattedBalance());
    }
}
