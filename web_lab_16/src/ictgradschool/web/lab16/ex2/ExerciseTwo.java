package ictgradschool.web.lab16.ex2;

import ictgradschool.web.lab16.Passwords;
import ictgradschool.web.lab16.ex1.ExerciseOne;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

public class ExerciseTwo {
    private static final char[] AVAILABLE_CHARS = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray(); //
    private Set<char[]> passwords = new LinkedHashSet<>();
    private byte[] expected = Passwords.base64Decode("ocxgyfryiU2BTwWn50EO6GF5me7gDjvoKvTFiRExsEkxc7qjr8dQxT/nyMfKBv+DSvajSXPsfFCR9zVg4v9Ldg==");

    private final int NUM_THREADS = 8;


    public static void main(String[] args) {
               Instant startTime = Instant.now();
        new ExerciseTwo().start();
        Duration duration = Duration.between(startTime, Instant.now());
        System.out.println(duration);
    }

    private void start() {
        for (int i = 3; i > 0; i--) {
            passwords.addAll(ExerciseOne.passwordGenerator(AVAILABLE_CHARS, i));
        }
        System.out.println(passwords.size());

        /* Multi-threading*/
//        Future[] futures = new Future[NUM_THREADS];
//        IntStream.range(0, NUM_THREADS).forEach(i -> futures[i] = es.submit(() -> crackPassword(isFound, i)));

        AtomicBoolean isFound = new AtomicBoolean();
        ExecutorService es = Executors.newCachedThreadPool();
        FutureTask[] futureTasks = IntStream.range(0, NUM_THREADS)
                .mapToObj(i -> new FutureTask<>(() -> crackPassword(isFound, i)))
                .toArray(FutureTask[]::new);
        Arrays.stream(futureTasks).forEach(es::execute);
        es.shutdown();
        Arrays.stream(futureTasks).map(ft -> {
            try {
                return ft.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                return null;
            }
        }).forEach(System.out::println);


        /* Single-threading*/
//        found[0] = "not found";
//        for (String password : passwords) {
//            //System.out.println(password);
//            for (byte i = Byte.MIN_VALUE; i != Byte.MAX_VALUE; i++) {
//                byte[] salt = new byte[]{i};
//                byte[] pw = Passwords.hash(password.toCharArray(), salt, 5);
//                if (pw == expected) {
//                    found[0] = password;
//                    System.out.println(found[0]);
//                }
//            }
//        }
//        System.out.println(found[0]);

    }

    private String crackPassword(AtomicBoolean isFound, int i) {
        for (Iterator<char[]> iterator = passwords.iterator(); iterator.hasNext(); ) {
            char[] password = iterator.next();
            byte lowerBound = (byte) (Byte.MIN_VALUE + (Byte.MAX_VALUE - Byte.MIN_VALUE) * (i / (double) NUM_THREADS));
            byte upperBound = (byte) (Byte.MIN_VALUE + (Byte.MAX_VALUE - Byte.MIN_VALUE) * ((i + 1) / (double) NUM_THREADS));
            for (byte idx = lowerBound; idx < upperBound; idx++) {
                byte[] salt = new byte[]{idx};
                //System.out.println(passwords.size());
                byte[] pw = Passwords.hash(password, salt, 5);
                if (Arrays.equals(pw, expected)) {
                    isFound.set(true);
                    return Arrays.toString(password);
                }
                if (isFound.get())
                    return "";
            }
        }
        return null;
    }
}
