package ictgradschool.web.lab16.ex1;

import ictgradschool.web.lab16.Passwords;

import java.util.*;

public class ExerciseOne {
    private static final char[] AVAILABLE_CHARS = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
    private Set<char[]> passwords = new LinkedHashSet<>();
    private byte[] expected = Passwords.base64Decode("wJmmp8BE3aqU8HpOQ8vVmtE9P0r6ZXh3uiHP+ZQx3CU9e6pcjD14ay7j+ljBs+gGicEcKS+pKdHn6VUpDGFgnQ==");

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

    private void start() {
        for (int i = 5; i > 0; i--) {
            passwords.addAll(passwordGenerator(AVAILABLE_CHARS, i));
        }

        String found = "not found";

        for (char[] password : passwords) {
            //System.out.println(password);
            if (Arrays.equals(Passwords.hash(password, null), expected))
                found = Arrays.toString(password);
        }

        System.out.println(found);

        byte[] pw = Passwords.insecureHash("aaaa");
        System.out.println(Passwords.base64Encode(pw));
    }

    public static Set<char[]> passwordGenerator(char[] possibleValues, int length) {
//        List<char[]> combinations = new ArrayList<>();
//        combination(AVAILABLE_CHARS, length, 0, 0, new BitSet(), combinations);
//        combinations.forEach(c -> permutation(c, 0, passwords));
        Set<char[]> passwords = new LinkedHashSet<>();
        boolean carry;
        int[] indices = new int[length];
        do {
            char[] pw = new char[length];
            for (int i = 0; i < indices.length; i++) {
                int index = indices[i];
                pw[i] = possibleValues[index];
            }
            passwords.add(pw);

            carry = true;
            for (int i = indices.length - 1; i >= 0; i--) {

                if (carry) indices[i]++;
                else break;

                carry = false;

                if (indices[i] == possibleValues.length) {
                    carry = true;
                    indices[i] = 0;
                }
            }
        }
        while (!carry); // Call this method iteratively until a carry is left over
        return passwords;
    }


    /**
     * Generate subsets of available digits, store result in 'combinations' field
     *
     * @param arr          super array
     * @param k            number of elements of sub array
     * @param start        pointer index of super array for current call
     * @param currLen      current length of the sub array
     * @param used         array of whether each element of super array to put into the sub array
     * @param combinations output
     */
    public static void combination(char[] arr, int k, int start, int currLen, BitSet used, List<char[]> combinations) {
        if (currLen == k) {
            char[] result = new char[k];
            int idx = 0;
            for (int i = 0; i < arr.length; i++) {
                if (used.get(i)) {
                    result[idx++] = arr[i];
                }
            }
            combinations.add(result);
            return;
        }

        if (start == arr.length) return;

        used.set(start);
        combination(arr, k, start + 1, currLen + 1, used, combinations);

        used.clear(start);
        combination(arr, k, start + 1, currLen, used, combinations);
    }

    /**
     * Generate all possible answers, store result in 'answers' field
     *
     * @param arr   original array to be permute
     * @param start pointer index of the array for current call
     */
    public static void permutation(char[] arr, int start, List<String> output) {
        if (start == arr.length - 1) {
            char[] onePermutation = Arrays.copyOf(arr, arr.length);
            output.add(String.valueOf(onePermutation));
        }
        for (int i = start; i < arr.length; i++) {
            swap(arr, start, i);
            permutation(arr, start + 1, output);
            swap(arr, start, i);
        }
    }

    public static void swap(char arr[], int x, int y) {
        char temp = arr[x];
        arr[x] = arr[y];
        arr[y] = temp;
    }
}
