package ictgradschool.industry.lab18.ex01;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("SameParameterValue")
public class Student {
    public enum ScoreType {
        Lab, Test, Exam,
    }
    private static final Map<ScoreType, Integer> NUM_SCORES = new HashMap<>();
    private static final Map<ScoreType, int[]> BREAK_POINTS = new HashMap<>();
    static {
        NUM_SCORES.put(ScoreType.Lab, 3);
        NUM_SCORES.put(ScoreType.Test, 1);
        NUM_SCORES.put(ScoreType.Exam, 11);
        BREAK_POINTS.put(ScoreType.Lab, new int[]{5, 15, 25, 65});
        BREAK_POINTS.put(ScoreType.Test, new int[]{5, 20, 65, 90});
        BREAK_POINTS.put(ScoreType.Exam, new int[]{7, 20, 60, 90});
    }

    static final Random r = new Random();
    private static final AtomicInteger numOfStudents = new AtomicInteger(0);
    private int id;
    private String firstName;
    private String surname;
    private final Map<ScoreType, Integer[]> scores = new TreeMap<>();

    /**
     * Student Factory
     */
    static Student create(String firstName, String surname) {
        return new Student(numOfStudents.incrementAndGet(), firstName, surname);
    }

    static Student[] toArray(int i) {
        return new Student[i];
    }

    private Student(int id, String firstName, String surname) {
        this.id = id;
        this.firstName = firstName;
        this.surname = surname;

        int randStudentSkill = r.nextInt(101);
        for (ScoreType scoreType : ScoreType.values()) {
            Integer[] eachTypeOfScores = new Integer[NUM_SCORES.get(scoreType)];
            Arrays.setAll(eachTypeOfScores, i -> generateScore(scoreType, randStudentSkill));
            scores.put(scoreType, eachTypeOfScores);
        }
    }

    private Integer generateScore(ScoreType scoreType, int skill) {
        if (BREAK_POINTS.get(scoreType).length != 4)
            throw new IllegalArgumentException("There should be 4 break points in " + scoreType);

        int score;
        if (skill <= BREAK_POINTS.get(scoreType)[0]) {
            if (scoreType == ScoreType.Exam) {
                int randDNSProb = r.nextInt(101);
                if (randDNSProb <= 5) return null;
            }
            score = r.nextInt(40); //[0,39]
        } else if (skill <= BREAK_POINTS.get(scoreType)[1]) {
            score = (r.nextInt(10) + 40); //[40,49]
        } else if (skill <= BREAK_POINTS.get(scoreType)[2]) {
            score = (r.nextInt(20) + 50); //[50,69]
        } else if (skill <= BREAK_POINTS.get(scoreType)[3]) {
            score = (r.nextInt(20) + 70); //[70,89]
        } else {
            score = (r.nextInt(11) + 90); //[90,100]
        }

        return score;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder(String.format("%04d\t%s\t%s\t", id, firstName, surname));
        for (ScoreType scoreType : ScoreType.values()) {
            for (Integer score : scores.get(scoreType)) {
                s.append(score != null ? score + "\t" : "\t");
            }
        }
        return s.toString().trim() + "\n";
    }
}
