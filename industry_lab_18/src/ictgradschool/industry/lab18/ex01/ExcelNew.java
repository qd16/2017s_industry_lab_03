package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import static ictgradschool.industry.lab18.ex01.Student.r;


/**
 * Please test & refactor this - my eyes are watering just looking at it :'(
 */
class ExcelNew {

    private static final int CLASS_SIZE = 55000;
    private final String OUT_FILE = "Data_out.txt";

    public static void main(String[] args) {
        new ExcelNew().start();
    }

    private void start() {
        String[] firstNames = readStringList("FirstNames.txt");
        String[] surnames = readStringList("Surnames.txt");
        Student[] students = Student.toArray(CLASS_SIZE);
        ExecutorService es = Executors.newFixedThreadPool(10);
        Future<Student[]> future = es.submit(() -> IntStream.range(0, CLASS_SIZE).forEach(i ->
                students[i] = Student.create(
                        firstNames[r.nextInt(firstNames.length)],
                        surnames[r.nextInt(surnames.length)])), students);
        es.shutdown();

        try {
            writeStudentRecord(future.get(), OUT_FILE);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private String[] readStringList(String fileName) {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            return br.lines().toArray(String[]::new);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String[]{};
    }

    private void writeStudentRecord(Student[] students, String fileName) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))) {
            for (Student s : students) {
                bw.write(s.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}