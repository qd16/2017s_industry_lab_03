package ictgradschool.industry.lab18.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Have fun :)
 */
class Program extends JFrame {

    private static final Random r = new Random();
    private static final int LEFT = 37;
    private static final int RIGHT = 39;
    private static final int UP = 38;
    private static final int DOWN = 40;
    private final int WIDTH = 30;
    private final int HEIGHT = 20;
    private final int INIT_LENGTH = 6;

    private final Canvas c = new Canvas();
    private Point food = new Point(WIDTH / 2, HEIGHT / 2);
    private final List<Point> snake = new ArrayList<>();
    private final List<Point> block = new ArrayList<>();
    private int dir = RIGHT;
    private boolean done = false;

    public static void main(String[] args) {
        new Program().go();
    }

    private Program() {
        setTitle("Program" + " : " + 6);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(200, 200, WIDTH * 25 + 6, HEIGHT * 25 + 28);
        setResizable(false);
        c.setBackground(Color.white);
        c.setBorder(new LineBorder(Color.black));
        add(BorderLayout.CENTER, c);

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int d1 = e.getKeyCode();
                if ((d1 >= 37) && (d1 <= 40)) {// block wrong codes
                    if (Math.abs(Program.this.dir - d1) != 2) {// block moving back
                        dir = d1;
                    }
                }
            }
        });
        setVisible(true);
    }

    private void go() { // main loop
        IntStream.range(0, INIT_LENGTH).forEach(i -> snake.add(new Point(INIT_LENGTH - i, HEIGHT / 2)));
        while (!done) {
            Point head = new Point(snake.get(0).X, snake.get(0).Y);
            move(head);                                  // Move
            done = checkCollision(head);                 // Check dead
            snake.add(0, head);                    // Head forward
            if (head.equals(food)) spawnFoodBlock();     // Add food and block
            else snake.remove(snake.size() - 1);   // Tail forward

            c.repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void move(Point head) {
        switch (dir) {
            case LEFT:
                head.X--;
                break;
            case RIGHT:
                head.X++;
                break;
            case UP:
                head.Y--;
                break;
            case DOWN:
                head.Y++;
                break;
        }
        if (head.X > WIDTH - 1) head.X = 0;
        if (head.X < 0) head.X = WIDTH - 1;
        if (head.Y > HEIGHT - 1) head.Y = 0;
        if (head.Y < 0) head.Y = HEIGHT - 1;
    }

    private void spawnFoodBlock() {
        setTitle("Program" + " : " + snake.size());
        Point p;
        do {
            p = new Point();
        } while (checkCollision(p));
        food = p;
        do {
            p = new Point();
        } while (checkCollision(p) || food.equals(p));
        block.add(p);
    }

    private boolean checkCollision(Point p) {
        boolean snakeTouched = IntStream.range(0, snake.size() - 1).anyMatch(i -> snake.get(i).equals(p));
        boolean blockTouched = block.stream().anyMatch(b -> b.equals(p));
        return blockTouched || snakeTouched;
    }

    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (Point p : snake) {
                g.setColor(Color.gray);
                g.fill3DRect(p.X * 25 + 1, p.Y * 25 + 1, 25 - 2, 25 - 2, true);
            }

            g.setColor(Color.green);
            g.fill3DRect(food.X * 25 + 1, food.Y * 25 + 1, 25 - 2, 25 - 2, true);

            for (Point p : block) {
                g.setColor(Color.red);
                g.fill3DRect(p.X * 25 + 1, p.Y * 25 + 1, 25 - 2, 25 - 2, true);
            }
            if (done) {
                g.setColor(Color.red);
                g.setFont(new Font("Arial", Font.BOLD, 60));
                FontMetrics fm = g.getFontMetrics();
                g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
            }
        }
    }

    class Point {
        int X;
        int Y;

        Point() {
            new Point(r.nextInt(WIDTH), r.nextInt(HEIGHT));
        }

        Point(int x, int y) {
            this.X = x;
            this.Y = y;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof Point && ((Point) obj).X == this.X && ((Point) obj).Y == this.Y;
        }
    }
}