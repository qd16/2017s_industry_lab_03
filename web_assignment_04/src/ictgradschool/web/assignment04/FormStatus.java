package ictgradschool.web.assignment04;

public enum FormStatus {
    Success, IOError, DuplicateRef, WrongCardNoFormat, NotVisa, NotMastercard, FileNotFoundError, InvalidCardNo
}
