package ictgradschool.web.assignment04;

import org.json.simple.JSONObject;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


/**
 * Servlet implementation class question1
 */
public class question1 extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String relativePath = "/Transactions";
    ;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public question1() {
        super();
    }

    /**
     * We set the directory that the transactions will be written to in the
     * during servlet configuration in web.xml
     */
    protected String transactionDir = null;

    /**
     * @param servletConfig a ServletConfig object containing information gathered when
     *                      the servlet was created, including information from web.xml
     */
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        this.transactionDir = servletConfig.getInitParameter("transaction-directory");
    }
    /** init(ServletConfig) => void **/


    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // You can uncomment the following line to check the Web server
        // if necessary
        //response.getWriter().append("Served at: ").append(request.getContextPath());

        PrintWriter out = response.getWriter();

        String refNum = request.getParameter("refNum");

        // If a transaction directory hasn't been set in the web.xml, then
        // default to one within our web context (which might be in some
        // strange directory deep within .metadata in the workspace)
        if (this.transactionDir == null) {
            ServletContext servletContext = getServletContext();
            this.transactionDir = servletContext.getRealPath(relativePath);
        }
        File jsonFile = new File(this.transactionDir, refNum + ".json");

        // Remove the following line, and then start coding to solve
        // the task described in Question 1 of the test
        out.println("<p>Location to save json file = " + jsonFile + "</p>");

        // Get data from form, save as JSON
        JSONObject jsonObject = getJsonObject(request);

        FormStatus status = saveJSONObject(jsonFile, jsonObject);
        switch (status) {
            case Success:
                out.println("<p>Data saved: OK</p>");
                break;
            case FileNotFoundError:
                out.println("<p>cannot find the path " + relativePath + " to save file.</p>");
            case IOError:
                out.println("<p>An IO error occurred.</p>");
                break;
            case DuplicateRef:
                out.println("<p>A Ref Number that has already been processed.</p>");
                break;
            case WrongCardNoFormat:
                out.println("<p>Wrong card number format, should be 16 digits.</p>");
                break;
            case NotVisa:
                out.println("<p>Not a Visa card number.</p>");
                break;
            case NotMastercard:
                out.println("<p>Not a Mastercard number.</p>");
                break;
            case InvalidCardNo:
                out.println("<p>Not a credit card number.</p>");
                break;
        }
    }

    /**
     * No special actions for POST so chains throught to doGet()
     *
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Auto-generated method stub
        doGet(request, response);
    }


    /**
     * Writes the given JSONObject (in JSON format) to the specified file path
     *
     * @param file       The file to save json
     * @param jsonRecord Data from form
     * @return FormStatus
     */
    private FormStatus saveJSONObject(File file, JSONObject jsonRecord) {

        if (file.exists()) {
            return FormStatus.DuplicateRef;
        }

        JSONObject card = (JSONObject) jsonRecord.get("creditCard");
        String cardNo = String.valueOf(card.get("cardNo")).replace("-", "");
        if (!cardNo.matches("^\\d{16}$")) {
            return FormStatus.WrongCardNoFormat;
        }

        if (card.get("cardType").equals("Visa") && !cardNo.matches("^4\\d{12}(\\d{3})?$")) {
            return FormStatus.NotVisa;
        }

        if (card.get("cardType").equals("Mastercard") && !cardNo.matches("^5[1-5]\\d{14}$")) {
            return FormStatus.NotMastercard;
        }

        if (!validateCreditCardNumber(cardNo)) {
            return FormStatus.InvalidCardNo;
        }

        String json_string = JSONObject.toJSONString(jsonRecord);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(json_string);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return FormStatus.FileNotFoundError;
        } catch (IOException e) {
            e.printStackTrace();
            return FormStatus.IOError;
        } finally {
            try {
                if (writer != null)
                    writer.close();
            } catch (IOException e) {
                e.printStackTrace();
                return FormStatus.IOError;
            }
        }

        return FormStatus.Success;
    }

    /**
     * Parse submitted form to json
     *
     * @return Json record
     */
    private JSONObject getJsonObject(HttpServletRequest request) {

        JSONObject billingAddress = new JSONObject();
        billingAddress.put("address", request.getParameter("address"));
        billingAddress.put("postCode", request.getParameter("postCode"));

        JSONObject creditCard = new JSONObject();
        creditCard.put("cardName", request.getParameter("cardName"));
        creditCard.put("cardType", request.getParameter("cardType"));
        creditCard.put("cardNo", request.getParameter("cardNo1") + "-" +
                request.getParameter("cardNo2") + "-" +
                request.getParameter("cardNo3") + "-" +
                request.getParameter("cardNo4"));

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("billingAddress", billingAddress);
        jsonObject.put("creditCard", creditCard);
        return jsonObject;
    }

    /**
     * Luhn Algorithm
     */
    private boolean validateCreditCardNumber(String str) {

        int[] ints = new int[str.length()];
        for (int i = 0; i < str.length(); i++) {
            ints[i] = Integer.parseInt(str.substring(i, i + 1));
        }
        for (int i = ints.length - 2; i >= 0; i = i - 2) {
            int j = ints[i];
            j = j * 2;
            if (j > 9) {
                j = j % 10 + 1;
            }
            ints[i] = j;
        }
        int sum = 0;
        for (int i = 0; i < ints.length; i++) {
            sum += ints[i];
        }
        return sum % 10 == 0;
    }
}